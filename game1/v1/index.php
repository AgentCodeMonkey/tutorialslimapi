<?php

// load slim
require '../../libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app->setName('Test Web API');

// load classes
spl_autoload_register('autoload_class');

function autoload_class($class_name) {
    $directories = array(
        '../include/',
        '../controller/',
        '../model/'
    );
    foreach ($directories as $directory) {
        $filename = $directory . $class_name . '.php';
        if (is_file($filename)) {
            // echo $filename . "  ";
            require($filename);
            break;
        }
    }
}

// Do nothing when we don't find an API endpoint
$app->notFound(function () {
    $statusCodeCon = new StatusCodeController();
    $statusCodeCon->ReturnStatusCode(true, NOT_FOUND, "404 not found");
});

// der erste Test: http://localhost/tutorialgameapi/game1/v1/hello/Hans
$app->get('/hello/:name', function ($name) {
    echo "Hello, $name";
});

// get player character by id or name
$app->get('/character/:guidorname', function($guidorname) use($app) {
    $cc = new CharacterController();
    $cc->GetCharacter($guidorname);
});

$app->post('/character/:name', /*'authentication',*/ function ($name) use($app) {
    VerifyRequiredParams(array('data'));
    $data = $app->request->put('data');
    $cc = new CharacterController();
    $cc->PostCharacter($name, $data);
});

// diese funktion ueberprueft ob es die felder gibt, nach denen gefragt wird
function VerifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = $_REQUEST;
    // Handling PUT request params

    if (\filter_input(INPUT_REQUEST, 'PUT')) {
        // if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $app = \Slim\Slim::getInstance();
        $statusCodeCon = new StatusCodeController();
        $statusCodeCon->ReturnStatusCode("true", "400", 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty');
        $app->stop();
    }
}

function authentication(\Slim\Route $route) {
    
    $app = \Slim\Slim::getInstance();
    // Verifying Authentication Header
    $statusCodeCon = new StatusCodeController();
    
    if (!isset($GLOBALS["headers"]['Account'])) {
        $statusCodeCon->ReturnStatusCode(true, AUTHENTICATION_FAILED, "Account failed");
        $app->stop();
    }
    if (!isset($GLOBALS["headers"]['Password'])) {
        $statusCodeCon->ReturnStatusCode(true, AUTHENTICATION_FAILED, "Authentication failed");
        $app->stop();
    }
    if (!isset($GLOBALS["headers"]['Apikey'])) {
        $statusCodeCon->ReturnStatusCode(true, APIKEY_CHECK_FAILED, "ApiKey failed");
        $app->stop();
    }
    if (!isset($GLOBALS["headers"]['Gameversion'])) {
        $statusCodeCon->ReturnStatusCode(true, VERSION_CHECK_FAILED, "Version failed");
        $app->stop();
    }
    if (!isset($GLOBALS["headers"]['Gameid'])) {
        $statusCodeCon->ReturnStatusCode(true, GAME_ID_FAILED, "Gameid failed");
        $app->stop();
    }
    if ($GLOBALS["headers"]['Apikey'] != API_KEY) { // falscher apikey
        $statusCodeCon->ReturnStatusCode(true, APIKEY_CHECK_FAILED, "wrong apikey! do you try to trick the server? next time we will bann you account.");
        $app->stop();
    }
    if ($GLOBALS["headers"]['Gameversion'] != VERSION) { // falsche version
        $statusCodeCon->ReturnStatusCode(true, VERSION_CHECK_FAILED, "Update your game!");
        $app->stop();
    }
    if ($GLOBALS["headers"]['Gameid'] != GAME_ID) { // falsche version
        $statusCodeCon->ReturnStatusCode(true, GAME_ID_FAILED, "Gameid failed");
        $app->stop();
    }
}

// WICHTIG, WICHTIG, WICHTIG: am Ende noch die App starten:
$app->run();
?>